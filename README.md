# Spelet #

Ett spel som använder sig av SpriteKit och Swift.

En boll styrs genom att användaren vrider på skärmen åt vänster eller höger. Målet är att komma så högt upp som möjligt genom att hoppa med bollen på plattformar.

Bollen kan inte styras i simulatorn eftersom att den inte har stöd för accelerometern. Det måste därför köras på en telefon för att fungera korrekt.

* Spelet fungerar från och med iOS 7.0
* Spelet fungerar på alla iPhone-storlekar