//
//  GameFile.swift
//  Spelet
//
//  Created by Pontus Melin on 2015-10-06.
//  Copyright © 2015 melin. All rights reserved.
//

import Foundation

class GameFile {
    var score: Int
    var highScore: Int
    
    class var sharedInstance: GameFile {
        struct Singleton {
            static let instance = GameFile()
        }
        
        return Singleton.instance
    }
    
    init() {
        score = 0
        highScore = 0
        
        // Laddar in poäng
        let defaults = NSUserDefaults.standardUserDefaults()
        
        highScore = defaults.integerForKey("highScore")
    }
    
    func saveState() {
        // Uppdaterar highscore
        highScore = max(score, highScore)
        
        // Sparar i userdefaults
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(highScore, forKey: "highScore")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}