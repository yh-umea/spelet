//
//  GameOverScene.swift
//  Spelet
//
//  Created by Pontus Melin on 2015-10-06.
//  Copyright © 2015 melin. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    var playAgainButton: SKNode! = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        let gameoverSound = SKAction.playSoundFileNamed("gameover.wav", waitForCompletion: false)
        runAction(gameoverSound)

            let background : SKSpriteNode = SKSpriteNode (imageNamed: "gameover")
            background.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2)
            background.size = self.frame.size
            self.addChild(background)
        
        // Score
        let lblScore = SKLabelNode(fontNamed: "Helvetica-Bold")
        lblScore.fontSize = 26
        lblScore.fontColor = SKColor.blackColor()
        lblScore.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        lblScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblScore.text = String(format: "Score: %d", GameFile.sharedInstance.score)
        addChild(lblScore)
        
        // High Score
        let lblHighScore = SKLabelNode(fontNamed: "Helvetica-Bold")
        lblHighScore.fontSize = 26
        lblHighScore.fontColor = SKColor.blackColor()
        lblHighScore.position = CGPoint(x: (self.size.width / 2)+5, y: (self.size.height / 2)-30)
        lblHighScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblHighScore.text = String(format: "High Score: %d", GameFile.sharedInstance.highScore)
        addChild(lblHighScore)
        
        // Play again
        playAgainButton = SKSpriteNode (imageNamed: "playagain")
        playAgainButton.position = CGPoint(x: self.size.width / 2, y: (self.size.height / 2)-60)
        addChild(playAgainButton)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            // Get the location of the touch in this scene
            let location = touch.locationInNode(self)
            // Check if the location of the touch is within the button's bounds
            if playAgainButton.containsPoint(location) {
                // Startar om spelet
                let reveal = SKTransition.fadeWithDuration(0.5)
                let gameScene = GameScene(size: self.size)
                self.view!.presentScene(gameScene, transition: reveal)
            }
        }
    }
}