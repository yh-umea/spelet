//
//  GameObjectNode.swift
//  Spelet
//
//  Created by Pontus Melin on 2015-10-05.
//  Copyright © 2015 melin. All rights reserved.
//

import SpriteKit


enum PlatformType: UInt32 {
    case Normal = 0
    case Break
    case SuperJump
    
    private static let _count: PlatformType.RawValue = {
        var maxValue: UInt32 = 0
        while let _ = PlatformType(rawValue: ++maxValue) { }
        return maxValue
    }()
    
    static func randomPlatform() -> PlatformType {
        let rand = arc4random_uniform(_count)
        return PlatformType(rawValue: rand)!
    }
}

struct CollisionCategoryBitmask {
    static let Ball: UInt32 = 0x00
    static let Platform: UInt32 = 0x02
}

class GameObjectNode: SKNode {
    func collisionWithBall(ball: SKNode) -> Bool {
        return false
    }
    
    func checkNodeRemoval(ballY: CGFloat) {
        if ballY > self.position.y + 300.0 {
            self.removeFromParent()
        }
    }
}

class PlatformNode: GameObjectNode {
    var platformType: PlatformType!
    
    override func collisionWithBall(ball: SKNode) -> Bool {
        
        // Bollen hoppar bara på plattformar när den faller
        if ball.physicsBody?.velocity.dy < 0 {
            ball.physicsBody?.velocity = CGVector(dx: ball.physicsBody!.velocity.dx, dy: 500.0)
            
            // Tar bort plattform om den är av typen "break"
            if platformType == .Break {
                self.removeFromParent()
            }
            if platformType == .SuperJump {
                self.removeFromParent()
                ball.physicsBody?.velocity = CGVector(dx: ball.physicsBody!.velocity.dx, dy: 800.0)
            }
        }

        return false
    }
}