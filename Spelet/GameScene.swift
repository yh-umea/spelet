//
//  GameScene.swift
//  Spelet
//
//  Created by Pontus Melin on 2015-09-28.
//  Copyright (c) 2015 melin. All rights reserved.
//

import SpriteKit
import CoreMotion

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // Bollens maxhöjd
    var maxBallY: Int!
    
    var startIndexY: Int!
    var maxIndexY: Int!
    
    // Accelerometer
    let motionManager = CMMotionManager()
    var xAcceleration: CGFloat = 0.0
    
    // Layered Nodes
    var backgroundNode: SKNode!
    var foregroundNode: SKNode!
    var hudNode: SKNode!
    
    // Tap To Start node
    let tapToStartNode = SKSpriteNode(imageNamed: "tapToStart")
    
    var ball: SKNode!
    var labelScore: SKLabelNode!
    var platformSprite: SKSpriteNode!
    
    var platformMinDelta: Int!
    
    // Gameovervyn visas inte
    var gameOver = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        backgroundColor = SKColor.whiteColor()
        
        // Bollen startar på 120, poängen startar därför på 120
        maxBallY = 120
        
        // Återställer poäng
        GameFile.sharedInstance.score = 0
        gameOver = false
        
        //Lägger till bakgrund
        backgroundNode = createBackgroundNode()
        addChild(backgroundNode)
        
        // Foreground
        foregroundNode = SKNode()
        addChild(foregroundNode)
        
        // Lägger till bollen
        ball = createBall()
        foregroundNode.addChild(ball)
        
        // Gravity
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -5.0)
        physicsWorld.contactDelegate = self
        
        // HUD
        hudNode = SKNode()
        addChild(hudNode)
        
        // Tap to Start
        tapToStartNode.position = CGPoint(x: self.size.width / 2, y: 300.0)
        hudNode.addChild(tapToStartNode)
        
        // CoreMotion
        motionManager.accelerometerUpdateInterval = 0.05
        motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: { accelerometerData, error in
            let acceleration = accelerometerData!.acceleration
            // Hastighet
            self.xAcceleration = (CGFloat(acceleration.x) * 1.2) + (self.xAcceleration * 0.4)
        })
        
        // Lägger till plattformar
        self.createPlatforms()
        
        // Visar poäng
        labelScore = SKLabelNode(fontNamed: "Helvetica-Bold")
        labelScore.fontSize = 26
        labelScore.fontColor = SKColor.whiteColor()
        labelScore.position = CGPoint(x: 15, y: self.size.height-40)
        labelScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        labelScore.text = "0"
        hudNode.addChild(labelScore)
        
        //Flyttar bollen framför plattformarna
        ball.zPosition = 1
        
    }
    
    //Funktion för bakgrunden
    func createBackgroundNode() -> SKNode {
        
        let backgroundNode = SKNode()

            let bg = SKSpriteNode(imageNamed: "background")
            bg.size = self.frame.size
            bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
            backgroundNode.addChild(bg)

        return backgroundNode
    }
    
    func createBall() -> SKNode {
        let ballNode = SKNode()
        ballNode.position = CGPoint(x: self.size.width / 2, y: 120.0)
        
        let ballSprite = SKSpriteNode(imageNamed: "ball")
        ballNode.addChild(ballSprite)
        
        // Fysik för bollen
        ballNode.physicsBody = SKPhysicsBody(circleOfRadius: ballSprite.size.width / 2.2)
        ballNode.physicsBody?.dynamic = false
        ballNode.physicsBody?.allowsRotation = false
        ballNode.physicsBody?.restitution = 1.0
        ballNode.physicsBody?.friction = 0.0
        ballNode.physicsBody?.angularDamping = 0.0
        ballNode.physicsBody?.linearDamping = 0.0
        
        ballNode.physicsBody?.usesPreciseCollisionDetection = true
        ballNode.physicsBody?.categoryBitMask = CollisionCategoryBitmask.Ball
        ballNode.physicsBody?.collisionBitMask = 0
        ballNode.physicsBody?.contactTestBitMask = CollisionCategoryBitmask.Platform
        
        return ballNode
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Om spelet är igång händer inget
        if ball.physicsBody!.dynamic {
            return
        }
        
        // Tar bort 'Tap to Start'
        tapToStartNode.removeFromParent()
        ball.physicsBody?.dynamic = true
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if ball.physicsBody?.velocity.dy < 0 {
            let platformSound = SKAction.playSoundFileNamed("platform.wav", waitForCompletion: false)
            runAction(platformSound)
        }
        var updateHUD = false
        let whichNode = (contact.bodyA.node != ball) ? contact.bodyA.node : contact.bodyB.node
        let other = whichNode as! GameObjectNode
        updateHUD = other.collisionWithBall(ball)
        if updateHUD {
            updateHUD = true
        }
    }
    
    
    func createPlatformAtPosition(position: CGPoint, ofType: PlatformType) -> PlatformNode {
        // 1
        let node = PlatformNode()
        node.name = "platformNode"
        node.platformType = PlatformType.randomPlatform()
        
        if (node.platformType == .Normal){
            // 3 olika storlekar på plattformar
            var platformImages = [SKSpriteNode]()
            platformImages.append(SKSpriteNode(imageNamed: "platformSmall"))
            platformImages.append(SKSpriteNode(imageNamed: "platformMedium"))
            platformImages.append(SKSpriteNode(imageNamed: "platform"))
            
            let randomPlatformImage = Int(arc4random_uniform(UInt32(platformImages.count)))
            platformSprite = platformImages[randomPlatformImage] as SKSpriteNode
            platformMinDelta = 50
        }
        else if (node.platformType == .Break){
            platformSprite = SKSpriteNode(imageNamed: "platformTransparent")
            platformMinDelta = 70
            
        }
        else if (node.platformType == .SuperJump){
            platformSprite = SKSpriteNode(imageNamed: "superJumpPlatform")
            platformMinDelta = 140
            
        }
 
        node.addChild(platformSprite)

        let platformPadding = platformSprite.size.width/2

        // Check that x position does not go above 'maxSpriteXPos'
        let maxSpriteXPos   = self.size.width-platformPadding
        
        // Check that x position does not go below 'minSpriteXPos'
        let minSpriteXPos   = platformPadding
        
        // Lägger första plattformen i mitten av skärmen
        if (position.y < 120){
             node.position = CGPoint(x: self.size.width/2, y: position.y)
            
        } else {
            // Slumpmässigt tal mellan 'minSpriteXPos' 'maxSpriteXPos'
            let actualPosX = self.randomFloatNumbers(minSpriteXPos, secondNum: maxSpriteXPos)
            node.position = CGPoint(x: actualPosX, y: position.y)
        }

        node.physicsBody = SKPhysicsBody(rectangleOfSize: platformSprite.size)
        node.physicsBody?.dynamic = false
        node.physicsBody?.categoryBitMask = CollisionCategoryBitmask.Platform
        node.physicsBody?.collisionBitMask = 0
        
        return node
    }
    
    // Funktion för slumpmässig tal mellan två floatvärden
    func randomFloatNumbers(firstNum: CGFloat, secondNum: CGFloat) -> CGFloat{
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNum - secondNum) + min(firstNum, secondNum)
    }

    
    override func update(currentTime: NSTimeInterval) {
        
        // Poängen ökar med bollens höjd
        if Int(ball.position.y) > maxBallY! {
            GameFile.sharedInstance.score += Int(ball.position.y) - maxBallY!
            maxBallY = Int(ball.position.y)
            labelScore.text = String(format: "%d", GameFile.sharedInstance.score)
        }
        
        // Tar bort plattformar som bollen hoppat förbi
        foregroundNode.enumerateChildNodesWithName("platformNode", usingBlock: {
            (node, stop) in
            let platform = node as! PlatformNode
            platform.checkNodeRemoval(self.ball.position.y)
        })
        
        // Calculate player y offset
        if ball.position.y > 200.0 {
            foregroundNode.position = CGPoint(x: 0.0, y: -(ball.position.y - 200.0))
        }

        
        // Om bollen faller 800 i y-led avslutas spelet
        if Int(ball.position.y) < maxBallY - 800 {
            endGame()
        }
        
        if gameOver {
            return
        }
        
        if Int(ball.position.y + 500) > maxIndexY! {
            self.createMorePlatforms()
        }
    }
    
    
    override func didSimulatePhysics() {
        ball.physicsBody?.velocity = CGVector(dx: xAcceleration * 400.0, dy: ball.physicsBody!.velocity.dy)
        if ball.position.x < -20.0 {
            ball.position = CGPoint(x: self.size.width + 20.0, y: ball.position.y)
        } else if (ball.position.x > self.size.width + 20.0) {
            ball.position = CGPoint(x: -20.0, y: ball.position.y)
        }
    }
    
    
    func endGame() {
        gameOver = true
        // Spara high score
        GameFile.sharedInstance.saveState()
        
        let reveal = SKTransition.fadeWithDuration(0.5)
        let endGameScene = GameOverScene(size: self.size)
        self.view!.presentScene(endGameScene, transition: reveal)
    }
    
    
    //Genererar slumpmässigt tal
    func randomInt(min min: Int, max: Int) -> Int {
        if max < min { return min }
        return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
    }
    
    
    func createPlatforms() {
        startIndexY = 50
        maxIndexY = 1000
        
        for var index = startIndexY; index < maxIndexY; index=self.newDelta(index, maxDelta: 110) {

            let x = self.randomInt(min: 0, max: Int(self.size.width))
            let y = index
            self.createPlatform(x, posY: y)
        }
    }
    
    func createMorePlatforms() {
        startIndexY = maxIndexY+20
        maxIndexY = maxIndexY+1000
        for var index = startIndexY; index < maxIndexY; index=self.newDelta(index, maxDelta: 100) {
            let x = self.randomInt(min: 0, max: Int(self.size.width))
            let y = index
            self.createPlatform(x, posY: y)
        }
    }
    
    func newDelta(oldValue: Int, maxDelta: Int) -> Int {
        return oldValue + self.randomInt(min: platformMinDelta, max: maxDelta)
    }
    
    func createPlatform(posX:Int, posY:Int) {
        let platform = createPlatformAtPosition(CGPoint(x: posX, y: posY), ofType: .Normal)
        foregroundNode.addChild(platform)
    }
}